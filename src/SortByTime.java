import java.util.Comparator;

class SortByTime implements Comparator<Car> {
    public int compare(Car a, Car b) {
        if ( a.getTime() < b.getTime() ) return -1;
        else if ( a.getTime() == b.getTime() ) return 0;
        else return 1;    }
}
