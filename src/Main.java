import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        Track track = new Track(1000, 1, 10, 25);
        Car[] cars = new Car[3];
        cars[0] = new Car(15, 100, "car1", 100, 0.1);
        cars[1] = new Car(46, 45, "car2", 50, 0.15);
        cars[2] = new Car(80, 10, "car3", 200, 0.05);

        for (Car c: cars) {
            System.out.println(c);
        }
        Tournament tournament = new Tournament(track, cars, 10);
        tournament.startTournament();

        for (Car c: cars) {
            System.out.println(c);
        }

        tournament.printResults();

        System.out.println(Tournament.res);
    }
}
