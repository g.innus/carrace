public class Result {
    private String carName;
    private Float carTime;

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Float getCarTime() {
        return carTime;
    }

    public void setCarTime(Float carTime) {
        this.carTime = carTime;
    }
}
