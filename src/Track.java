public class Track {
    private double lapDistance;
    private int laps;
    private int tyreLose = 1;
    private double gasLose  = 20;

    public Track(double lapDistance, int laps, int tyreLose, double gasLose) {
        this.lapDistance = lapDistance;
        this.laps = laps;
        this.tyreLose = tyreLose;
        this.gasLose = gasLose;
    }

    public double getLapDistance() {
        return lapDistance;
    }

    public void setLapDistance(double lapDistance) {
        this.lapDistance = lapDistance;
    }

    public int getLaps() {
        return laps;
    }

    public void setLaps(int laps) {
        this.laps = laps;
    }

    public void doLap(Car car){

        if (car.getGasoline()<gasLose||car.getTyreHelth()<tyreLose) {
            car.doPitstop();
        }
        car.setGasoline(car.getGasoline()-gasLose);
        car.setTyreHelth(car.getTyreHelth()-tyreLose);

        car.setTime(car.getTime()+lapDistance/car.getSpeed());
        Tournament.addResult(car.getName(), lapDistance/car.getSpeed());
        System.out.println(Tournament.res);


    }
}
