import java.util.Collections;

public class Car{
    private int tyreHelth;
    private double gasoline;
    private String name;
    private double speed;
    private double pitstop;
    private double time;

    public Car(int tyreHelth, double gasoline, String name, double speed, double pitstop) {
        this.tyreHelth = tyreHelth;
        this.gasoline = gasoline;
        this.name = name;
        this.speed = speed;
        this.pitstop = pitstop;
        time = 0;
    }

    public int getTyreHelth() {
        return tyreHelth;
    }

    public void setTyreHelth(int tyreHelth) {
        this.tyreHelth = tyreHelth;
    }

    public double getGasoline() {
        return gasoline;
    }

    public void setGasoline(double gasoline) {
        this.gasoline = gasoline;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public double getPitstop() {
        return pitstop;
    }

    public void setPitstop(double pitstop) {
        this.pitstop = pitstop;
    }

    public void doPitstop(){
        tyreHelth = 100;
        gasoline = 100;
        setTime(time+pitstop);
        Tournament.addResult(name, pitstop);
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Car{" +
                "tyreHelth=" + tyreHelth +
                ", gasoline=" + gasoline +
                ", name='" + name + '\'' +
                ", speed=" + speed +
                ", pitstop=" + pitstop +
                ", time=" + time +
                '}';
    }


}
