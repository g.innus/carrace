import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Tournament {
    private Track track;
    private Car[] cars;
    private int laps;
    static HashMap<String, Double> res = new HashMap<String, Double>();


    static void addResult(String name, double time){
        double oldTime = 0;
        if(res.get(name)!=null) {
            oldTime = res.get(name);
        }
        res.put(name, time+oldTime);
    }

    public Tournament(Track track, Car[] cars, int laps) {
        this.track = track;
        this.cars = cars;
        this.laps = laps;
    }

    public void startTournament(){
        for (Car car:cars) {
            for (int i = 0; i < laps; i++) {
                track.doLap(car);
            }
        }
    }
    public void printResults(){
        Arrays.sort(cars, new SortByTime());
        int i = 1;
        for (Car c : cars){
            System.out.printf("%d place %s with time %f", i, c.getName(), c.getTime());
            System.out.println();
            i++;
        }

    }


}
